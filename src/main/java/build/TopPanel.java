package build;

import com.opencsv.CSVReader;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

/**
 * Classe générant le JPanel top
 *
 */
public class TopPanel extends JPanel implements ActionListener {

    JMenuItem open;
    JMenuItem quit;
    CenterPanel c;

    /**
     * Constructeur paramétré
     *
     * @param center build.CenterPanel
     */
    public TopPanel(CenterPanel center) {
        super();
        c = center;
        affichageMenu();
        this.setVisible(true);
    }

    /**
     * Méthode pour afficher le barre de menu
     *
     */
    private void affichageMenu() {

        JMenuBar mb=new JMenuBar();

        JMenu file = new JMenu("Fichier");
        JMenu edit = new JMenu("Édition");

        quit = new JMenuItem("Quitter");
        open = new JMenuItem("Ouvrir");

        quit.addActionListener(this);
        open.addActionListener(this);

        edit.add(open);
        file.add(quit);

        mb.add(file);
        mb.add(edit);
        this.add(mb);
    }

    /**
     * Méthode ouvrant un CSV et l'affichant dans le JPanel centre
     *
     */
    public void openFile(){
        JFileChooser fileChooser = new JFileChooser();

        fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
        FileNameExtensionFilter filter = new FileNameExtensionFilter("CSV documents", "csv", "excel");
        fileChooser.setFileFilter(filter);

        fileChooser.setAcceptAllFileFilterUsed(false);

        int result = fileChooser.showOpenDialog(this);
        if (result == JFileChooser.FILES_ONLY) {
            File selectedFile = fileChooser.getSelectedFile();

            Object[] columnnames;
            CSVReader CSVFileReader = null;
            try {
                CSVFileReader = new CSVReader(new FileReader(selectedFile));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            List myEntries = null;
            try {
                myEntries = CSVFileReader.readAll();
            } catch (IOException e) {
                e.printStackTrace();
            }
            columnnames = (String[]) myEntries.get(0);
            DefaultTableModel tableModel = new DefaultTableModel(columnnames, myEntries.size()-1);

            int rowcount = tableModel.getRowCount();
            int colcount = tableModel.getColumnCount();
            Object[][] tabData = new Object[rowcount][colcount];

            for (int x = 0; x<rowcount+1; x++)
            {
                int columnnumber = 0;
                // x=0 --> Column headers
                if (x>0)
                {
                    for (String thiscellvalue : (String[])myEntries.get(x))
                    {
                        tabData[x-1][columnnumber] = thiscellvalue;
                        columnnumber++;
                    }
                }
            }

            this.c.add(new JScrollPane(new JTable(tabData, columnnames)));
            this.c.revalidate();
            this.c.repaint();
            
        }
    }

    /**
     * Action des boutons de la JMenuBar
     *
     * @param e Origine de l'évènement
     */
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==quit) System.exit(0);
        if(e.getSource()==open) openFile();
    }


}
