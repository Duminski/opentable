package build;

import javax.swing.*;
import java.awt.*;

/**
 * Classe générant la fenêtre
 *
 */
public class Graphique extends JFrame {

    private TopPanel top;
    private CenterPanel center;

    /**
     * Création de la fenêtre
     *
     * @param title Titre de la frame
     */
    public Graphique(String title){
        super(title);
        this.setSize(800,800);

        this.center = new CenterPanel();
        this.add(this.center, BorderLayout.CENTER);

        this.top = new TopPanel(center);
        this.add(this.top, BorderLayout.NORTH);

        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setVisible(true);

    }

    /**
     * Getter panel Top
     * @return build.TopPanel
     */
    public TopPanel getTop() {
        return top;
    }

    /**
     * Getter panel Center
     * @return build.CenterPanel
     */
    public CenterPanel getCenter() {
        return center;
    }

}
